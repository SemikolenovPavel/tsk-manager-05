package ru.t1.semikolenov.tm;

import ru.t1.semikolenov.tm.constant.TerminalConst;

import java.util.Arrays;

public final class Application {

    public static void main(final String[] args) {
        process(args);
    }

    public static void process(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            default:
                showError(arg);
                break;
        }
    }

    public static void showError(String arg) {
        System.err.printf("Error! This argument '%s' not supported...  \n", arg);
        System.exit(0);
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Pavel Semikolenov");
        System.out.println("E-mail: pavelsemikolenov@yandex.ru");
        System.exit(0);
    }

    public static void showVersion() {
        System.out.println("1.5.2");
        System.exit(0);
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - Show developer info. \n", TerminalConst.ABOUT);
        System.out.printf("%s - Show application version. \n", TerminalConst.VERSION);
        System.out.printf("%s - Show terminal commands. \n", TerminalConst.HELP);
        System.exit(0);
    }

}
